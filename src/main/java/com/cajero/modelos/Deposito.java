/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cajero.modelos;

import java.sql.SQLException;

/**
 *
 * @author frey
 */
public class Deposito extends Transaccion {

    @Override
    public void registrarTransaccion() throws ClassNotFoundException, SQLException {
        this.setTipo("Deposito");// instancia actual.
        super.registrarTransaccion();// ejecuta el método del padre.
    }
    
}
