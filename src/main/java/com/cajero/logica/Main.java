/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cajero.logica;

import com.cajero.modelos.Cuenta;
import com.cajero.modelos.Deposito;
import com.cajero.modelos.Estudiante;
import com.cajero.modelos.Retiro;
import java.sql.SQLException;
import java.util.Scanner;

/**
 *
 * @author frey
 */
public class Main {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
       
        Scanner s = new Scanner(System.in);

        System.out.println("Software de Cajero");
        System.out.println("Ingrese su cedula:");
        int cedula = s.nextInt();

        Estudiante e = new Estudiante(); // instancia
        e.setId(cedula); 

        // Chequeamos si existe el estudiante en la DB
        if (e.consultar()) {
            // Existe
            int opcion = -1; // bandera

            Cuenta c = new Cuenta(); // Instanciamos nuestro modelo cuenta
            c.setIdEstudiante(e.getId()); // Ponemos el id_estudiante en la instancia de cuenta c
            c.consultar(); // Traemos los datos de la tabla cuentas para este estudiante

            while (opcion != 0) {
                System.out.println("Seleccione una opción");
                System.out.println("    1. Consultar Saldo");
                System.out.println("    2. Depositar");
                System.out.println("    3. Retirar");
                System.out.println("    0. Salir");

                opcion = s.nextInt();
                if (opcion >= 0 && opcion <= 3) {
                    switch (opcion) { // opcion = 2
                        case 1:
                            System.out.println(c.getSaldo() + " Pesos.");
                            break;
                        case 2:
                            System.out.println("------------------------------");
                            System.out.println("Digite la cantidad a depositar");
                            System.out.println("------------------------------");
                            float saldoDepositar = s.nextFloat(); // pedimos la cantidad a depositar
                            c.setSaldo(c.getSaldo() + saldoDepositar);
                            c.actualizar(); // ejecutar el metodo actualizar
                            Deposito d = new Deposito(); // Instancia
                            d.setValor(saldoDepositar);
                            d.registrarTransaccion();
                            System.out.println(c.getSaldo() + " Pesos.");
                            break;
                        case 3:
                            System.out.println("Digite la cantidad a retirar");
                            float saldoRetirar = s.nextFloat();
                            c.setSaldo(c.getSaldo() - saldoRetirar);
                            c.actualizar();
                            Retiro r = new Retiro();
                            r.setValor(saldoRetirar);
                            r.registrarTransaccion();
                            System.out.println("$ " + c.getSaldo() + " Pesos.");
                            break;
                    }
                }else{
                    System.out.println("-----------------------------------");
                    System.out.println("Opción erronea, vuelva a intentarlo");
                    System.out.println("-----------------------------------");
                }
            }

        } else {
            // No Existe
            System.out.println("------------------------------------------------");
            System.out.println("Lo Sentimos, el usuario no existe en el sistema.");
            System.out.println("------------------------------------------------");
        }

    }

}
