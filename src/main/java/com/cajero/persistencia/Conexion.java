/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cajero.persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Tefa
 */
public class Conexion {
    
    // Atributos
    private static String url="jdbc:mysql://localhost:3306/mintic2021";
    private static String user="root";
    private static String password= "123456789";
    
    // Métodos
    public static Connection iniciarConexion() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection;
        connection = DriverManager.getConnection(url,user,password);
        System.out.println("Connection is Successful to the database "+url);
        return connection;         
    }
    
    // Método
    public static ResultSet ejecutarConsulta(String sql) throws ClassNotFoundException, SQLException{        
        Connection conex = iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
        if(sql.toUpperCase().startsWith("SELECT")){
            return statement.getResultSet();
        } else if (sql.toUpperCase().startsWith("UPDATE")){
            return null;
        } else if (sql.toUpperCase().startsWith("DELETE")){
            return null;
        } else if (sql.toUpperCase().startsWith("INSERT")){
            return null;
        }
        statement.close(); //Cerramos 
        return null;
    }
    
}
